# GW data prep 1 : depth and Color Synchronization
# Created by global walker Inc. Japan
# This code is for data collected via realsense-viewer in ros .bag is required
# To be used with Data Version 3 to Version 5
# Did not store pure depth information, normalize to 0:255
# Data preparation step 1

import csv
import os
from itertools import groupby
from shutil import copyfile

import cv2
import numpy as np
import pyrealsense2 as rs

# import rawpy

# color_path = '/media/zeyan/SSD-PGMU3/Work/Dataset/GW_data/others/submitted_data/GW_data/ver4/zeyan/zeyan_out_obj2'
target="person3"

pre_path = f"/media/gw-shinohara/Dev2Partition1/fallen_image_from_realsense/Large_meeting_room/{target}"

internal_path_list = os.listdir(pre_path)

# ros_file_path = (
#     # "/media/zeyan/gwSSD1/CSP_action/data/data_rs/data_collect/test_drunk_1.bag"
#     "/media/masato/Dev2Partition1/csp0810_6m_sakai/000/20220810_021121.bag"
# )
# out_path = "/home/zeyan/Documents/Work/GW-pose/utils/out"
out_path_pre= f"./output/{target}"

# path_list = os.splitpath()


recorded_FPS = 5
R_FPS = 5  # Required fps output
num = recorded_FPS / R_FPS

resize = False

for inter in internal_path_list:
    if not os.path.exists(os.path.join(out_path_pre, inter)):
        os.makedirs(os.path.join(out_path_pre, inter))
    endpioint_list = os.listdir(os.path.join(pre_path, inter))
    for endpoint in endpioint_list:
        ros_file_path = pre_path + '/' + inter + '/' + endpoint
        out_path = out_path_pre + '/' + inter + '/' + endpoint.split('.')[0]
        if not os.path.exists(out_path):
            os.makedirs(os.path.join(out_path))
        if not os.path.exists(os.path.join(out_path, "color")):
            os.makedirs(os.path.join(out_path, "color"))
        if not os.path.exists(os.path.join(out_path, "depth")):
            os.makedirs(os.path.join(out_path, "depth"))


        ##  Get Realsense Camera metadata from recorded ros. bag file  ##

        ## Initialize
        pipeline = rs.pipeline()
        config = rs.config()
        counter = 0
        # config.enable_stream(rs.stream.depth, 1280 , 720, rs.format.z16, 30) # dimension
        # config.enable_stream(rs.stream.color, 1280, 720, rs.format.rgb8, 30)
        config.enable_device_from_file(ros_file_path)
        profile = pipeline.start(config)

        ## Find Total number of frames
        playback = profile.get_device().as_playback()
        playback.set_real_time(False)
        # get the duration of the video
        t = playback.get_duration()
        # compute the number of frames (30fps setting)
        # frame_counts = t.seconds * 30
        frame_counts = t.seconds * 5
        print("Total_frame_count:", frame_counts)

        ## Get Depath sensor metadata information
        depth_sensor = profile.get_device().first_depth_sensor()

        depth_scale = depth_sensor.get_depth_scale()
        print(depth_scale)
        align_to = rs.stream.color
        align = rs.align(align_to)
        counter = 0
        try:
            while True:
                frames = pipeline.wait_for_frames()
                frames = align.process(frames)  # Synchronization  1
                depth_frame = frames.get_depth_frame()
                color_frame = frames.get_color_frame()
                # get color and depth_time_stamp
                depth_meta = depth_frame.get_frame_number()
                color_meta = color_frame.get_frame_number()
                flag = divmod(counter, num)  # required FPS output
                if not depth_frame or not color_frame:  # Synchronization2
                    continue
                print(flag[1])
                if flag[1] == 0:

                    print("time_stamp:", counter)
                    ##Process Color Image
                    color_image = np.asanyarray(color_frame.get_data())
                    im_bgr = cv2.cvtColor(color_image, cv2.COLOR_RGB2BGR)
                    # Process Depth Image
                    depth_image = np.asanyarray(depth_frame.get_data())
                    depth_conv = cv2.convertScaleAbs(depth_image, alpha=0.03)
                    depth_conv3D = np.dstack((depth_conv, depth_conv, depth_conv))
                    depth_image_3d = np.dstack((depth_image, depth_image, depth_image))
                    # depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
                    cv2.imwrite(
                        os.path.join(out_path, "color", "_Color_" + str(color_meta) + ".png"),
                        im_bgr,
                    )
                    cv2.imwrite(
                        os.path.join(out_path, "depth", "_Depth_" + str(color_meta) + ".png"),
                        depth_image_3d,
                    )
                if counter == frame_counts:
                    break
                counter = counter + 1
        finally:
            pipeline.stop()
