import image_converter

dir_name ="/media/gw-shinohara/Dev2Partition1/CSP_jetson/realsense_data/Large_meeting_room-20221219T071624Z/Large_meeting_room/meters3/bending_and_stretching_pose/20221216_182202"
input_dir=fr"{dir_name}/depth"
out_dir  =fr"{dir_name}/convert_depth"

import PathGetter
file_list=PathGetter.getNames(input_dir)
for file in file_list:
  image_converter.main(path=f"{input_dir}/{file}", out_dir=out_dir)