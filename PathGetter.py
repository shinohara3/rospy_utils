#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc

import os

def parserGetPaths():
  import argparse
  parser = argparse.ArgumentParser('Get path program')
  parser.add_argument('-i', '--input', help='input directory path', type=str, required=True)
  parser.add_argument('-o', '--output', help='output directory path', type=str, default=None)
  parser.add_argument('-n', '--name', help='output paths text basename', type=str, default=None)
  parser.add_argument('-e', '--ext', nargs='+', help='list of ext targets', default=None)
  args = parser.parse_args()
  return args

def getMatchesForExt( filenames:list, targets:list)-> list: 
  matchesFilenames=[]
  for file in filenames:
    base, ext=os.path.splitext(file)
    if ext in set(targets):
      matchesFilenames.append(file)
  return matchesFilenames

def getNames( dir:str )-> list: 
  return os.listdir(dir)

def writePathsList(outputDir:str, filenames:list, name:str)->bool:
  os.makedirs(outputDir, exist_ok=True)
  with open(file=fr'{outputDir}/{name}.txt', mode='w') as f:
      f.write('\n'.join(filenames))

if __name__ == "__main__":
  ## This is not tested ##
  arg=parserGetPaths()
  print(f"Get paths: {arg.input}\n")
  filenames=getNames(arg.input)

  if arg.ext is not None:
    print(f"Get matches for ext: {arg.ext}\n")
    filenames=getMatchesForExt(filenames, targets=arg.ext )
  output_dir=arg.output
  name="filenames"
  if arg.name is not None:
    name=arg.name
  if output_dir is not None:
    writePathsList(output_dir, filenames, name)
    print(f"Write paths list to {output_dir}/{name}.txt")
    