#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc

import rospy

def caluculateProcessTime(func):
  def wrapper(*args, **kwargs):
    start=rospy.get_time()
    result=func(*args, **kwargs)
    end=rospy.get_time()
    diff=end-start
    try:
      fps=1/(diff)
    except ZeroDivisionError as e:
      rospy.logerror("ZeroDivisionError")
    rospy.loginfo(f"{func.__name__}, fps:{fps}f/s, diff time: {diff}s")
    return result
  return wrapper
