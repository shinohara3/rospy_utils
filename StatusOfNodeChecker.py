#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc

import rospy
from detect_object.msg import StatusOfNode

class StatusOfNodeChecker():
  def __init__(self, node_name:str):
    self.__previous_status = None
    self.__msg = StatusOfNode()
    self.__msg.status=None
    self.__msg.message=node_name
    self.__log_publisher=rospy.Publisher(r"/StatusOfNodeChecker", StatusOfNode, queue_size=1)

  def setStatusError(self):
    self.__msg.status=StatusOfNode.TYPE_ERROR
  
  def setStatusNormal(self):
    self.__msg.status=StatusOfNode.TYPE_NORMAL
    if self.__previous_status is StatusOfNode.TYPE_ERROR:
      self.__setStatusRecovery()

  def __setStatusRecovery(self):
    self.__msg.status=StatusOfNode.TYPE_RECOVERY

  def publishStatus(self, log_func=rospy.loginfo):
    log_func(f"{self.__msg.message} node status: {self.__msg.status}")
    self.__log_publisher.publish(self.__msg)
    self.__previous_status=self.__msg.status
