#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc

from .rospy_message_converter.message_converter import convert_dictionary_to_ros_message
import yaml
import json
import os

class MessageLoader:
  def __init__(self):
    pass
  
  @classmethod
  def loadMessage(cls, path)->dict:
    _, basename = os.path.split(path)
    _, ext_type=os.path.splitext(basename)
    match ext_type:
      case ".json":
        dictionary=cls.__loadJson(path)
      case ".yaml":
        dictionary=cls.__loadYaml(path)
      case ext:
        raise Exception(f'Ext type "{ext}" is not defined.')
    return dictionary
  
  @classmethod
  def convert_dict_to_msg(cls, dictionary:dict, message_class):
    return convert_dictionary_to_ros_message(message_class, dictionary)

  @classmethod
  def __loadJson(cls, path:str)->dict:
    with open(path) as f:
      file = json.load(f)
    return file

  @classmethod
  def __loadYaml(cls, path:str)->dict:
    with open(path) as f:
      file = yaml.safe_load(f)
    return file
    