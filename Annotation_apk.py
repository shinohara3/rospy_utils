#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc

import tkinter as tk

class ImageTransitionApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.image_files = [
          "/home/gw-shinohara/Pictures/Screenshot from 2022-10-31 10-54-40.png", 
          '/home/gw-shinohara/Pictures/Screenshot from 2022-11-01 14-15-49.png', 
          '/home/gw-shinohara/Pictures/Screenshot from 2022-11-11 18-39-30.png']
        self.current_image_index = 0
        self.label = tk.Label(self, image=self.get_current_image())
        self.label.pack()
        self.button_left=tk.Button(self, text='<', command=self.show_prev_image)
        self.button_left.pack(side='left')
        self.button_right=tk.Button(self, text='>', command=self.show_next_image)
        self.button_right.pack(side='right')

    def get_current_image(self):
        self.__current_image=tk.PhotoImage(file=self.image_files[self.current_image_index])
        return self.__current_image

    def show_prev_image(self):
        self.current_image_index -= 1
        if self.current_image_index < 0:
            self.current_image_index = len(self.image_files) - 1
        self.label.configure(image=self.get_current_image())

    def show_next_image(self):
        self.current_image_index += 1
        if self.current_image_index >= len(self.image_files):
            self.current_image_index = 0
        self.label.configure(image=self.get_current_image())

if __name__ == '__main__':
  app = ImageTransitionApp()
  try:
    app.mainloop()
  except KeyboardInterrupt as e:
    app.destroy()

  