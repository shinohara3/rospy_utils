#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc

import cv2
import os

def readImage(path:str):
  if not os.path.exists(path):
    print(f"{path} is Not Found.")
    raise FileNotFoundError
  try:
      image_bgr = cv2.imread(path)
      return image_bgr
  except cv2.error as e:
    print(e)
    print('Image can not write.')
    return None

def readImageRGB(path:str):
  image_bgr=readImage(path)
  if image_bgr is None:
    return image_bgr
  image_rgb=cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)
  return image_rgb

def writeImage(image, path:str)->bool:
  try:
    cv2.imwrite(path, image)
  except cv2.error as e:
    print(e)
    print('Image can not write.')
    return False
  return True
