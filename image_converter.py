#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc

# CV_16UC1

import cv2
import os
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

def convert_rgb_to_gray(image):
    # Extract the blue channel from the image
    grayscale_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return grayscale_image

def convert_gray_to_pseudo_color(image):
    pseudo_color = cv2.applyColorMap(image, cv2.COLORMAP_HSV)
    return pseudo_color

def parserGetPaths():
  import argparse
  parser = argparse.ArgumentParser('Get path program')
  parser.add_argument('-p', '--input_path', help='Input image path.', type=str, required=False, default=None)
  parser.add_argument('-i', '--input_dir', help='Input image dir.', type=str, required=False, default=None)
  parser.add_argument('-o', '--output_dir', help='Output image directory.', type=str, required=False, default="./")
  args = parser.parse_args()
  return args

def main(path:str, out_dir:str):
  # Load an image in RGB8 format
  image = cv2.imread(path, cv2.IMREAD_ANYDEPTH)
  #image_pseudo_color = convert_gray_to_pseudo_color(image)
  basename=os.path.basename(path)

  os.makedirs(out_dir, exist_ok=True)
  out_path=f"{out_dir}/{basename}"
  print(f"out_image: {out_path}")
  # Save the converted image
  #cv2.imwrite(out_path, image_grayscale)
  vmin=0
  vmax=8000
  plt.imshow(image, cmap="jet", vmin=vmin, vmax=vmax)
  plt.colorbar(shrink=0.62)
  plt.savefig(out_path)
  plt.close()


if __name__ == "__main__":
  arg=parserGetPaths()
  out_dir=arg.output_dir

  if arg.input_path is not None:
    print(f"Get paths: {arg.input_path}\n")
    path=arg.input_path
    main(path, out_dir)

  if arg.input_dir is not None:
    input_dir=arg.input_dir
    import PathGetter
    file_list=PathGetter.getNames(input_dir)
    for file in file_list:
      main(path=f"{input_dir}/{file}", out_dir=out_dir)