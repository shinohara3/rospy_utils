#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc
import rospy
from std_msgs.msg import Header

class HeaderMaker():
  def __init__(self):
    self.__header = Header()
    self.__counter=0

  def setHeader(self, fill_num=6):
    self.__counter+=1
    self.__header.seq+=1
    self.__header.stamp = rospy.Time.now()
    self.__header.frame_id=f"{self.__counter}".zfill(fill_num)

  def getHeader(self):
    return self.__header
