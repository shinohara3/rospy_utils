#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Global Walkers.inc
from .rospy_message_converter.message_converter import convert_ros_message_to_dictionary
import yaml
import json
import os
class MessageWriter:
  def __init__(self):
    pass
  
  @classmethod
  def writeMessage(cls, message, result_out_dir:str, basename:str, ext_type:str=".json", indent:int=4):
    dictionary=convert_ros_message_to_dictionary(message)
    os.makedirs(result_out_dir, exist_ok=True)
    path=fr"{result_out_dir}/{basename}{ext_type}"
    if ext_type==".json":
      cls.__writeJson(path, dictionary, indent)
    elif ext_type==".yaml":
      cls.__writeYaml(path, dictionary, indent)
    else:
      raise Exception(f'Ext type {ext_type} is not defined.')

  @classmethod
  def __writeJson(cls, path, dictionary, indent):
    with open(path, 'w') as f:
      json.dump(dictionary, f, indent=indent)

  @classmethod
  def __writeYaml(cls, path, dictionary:dict, indent):
    with open(path, 'w') as f:
      yaml.dump(dictionary, f, indent=indent)